// Завдання

// Створити сторінку, яка імітує стрічку новин соціальної мережі Twitter.


// Технічні вимоги:

// При відкритті сторінки необхідно отримати з сервера список всіх користувачів 
// та загальний список публікацій. Для цього потрібно надіслати GET запит на наступні дві адреси:

// https://ajax.test-danit.com/api/json/users
// https://ajax.test-danit.com/api/json/posts


// Після завантаження всіх користувачів та їх публікацій необхідно відобразити всі публікації на сторінці.
// Кожна публікація має бути відображена у вигляді картки (приклад в папці), 
// та включати заголовок, текст, а також ім'я, прізвище та імейл користувача, який її розмістив.
// На кожній картці повинна бути іконка або кнопка, яка дозволить видалити цю картку зі сторінки. 
// При натисканні на неї необхідно надіслати DELETE запит на адресу 
// https://ajax.test-danit.com/api/json/posts/${postId}. 
// Після отримання підтвердження із сервера (запит пройшов успішно), 
// картку можна видалити зі сторінки, використовуючи JavaScript.
// Більш детальну інформацію щодо використання кожного з цих зазначених вище API можна знайти тут.
// Цей сервер є тестовим. Після перезавантаження сторінки всі зміни, які надсилалися на сервер, 
// не будуть там збережені. Це нормально, все так і має працювати.
// Картки обов'язково мають бути реалізовані у вигляді ES6 класів. Для цього необхідно створити клас Card. 
// При необхідності ви можете додавати також інші класи.


// Необов'язкове завдання підвищеної складності

// Поки з сервера під час відкриття сторінки завантажується інформація, показувати анімацію завантаження. 
// Анімацію можна використовувати будь-яку. Бажано знайти варіант на чистому CSS без використання JavaScript.
// Додати зверху сторінки кнопку Додати публікацію. При натисканні на кнопку відкривати модальне вікно, 
// в якому користувач зможе ввести заголовок та текст публікації. 
// Після створення публікації дані про неї необхідно надіслати в POST запиті на адресу:  
// https://ajax.test-danit.com/api/json/posts. Нова публікація має бути додана зверху сторінки 
// (сортування у зворотному хронологічному порядку). Автором можна присвоїти публікації користувача з id: 1.
// Додати функціонал (іконку) для редагування вмісту картки. Після редагування картки 
// для підтвердження змін необхідно надіслати PUT запит на адресу 
// https://ajax.test-danit.com/api/json/posts/${postId}.

import { Post, User } from "./class.js";

const API_USER_URL = "https://ajax.test-danit.com/api/json/users";
const API_POST_URL = "https://ajax.test-danit.com/api/json/posts";

const newsContainer = document.getElementById('newsContainer');
const posts = []; //Post
const users = []; //User
let authenticatedUser = undefined;
const editModal = document.getElementById("editModal");
const postModal = document.getElementById('postModal');
const addPostBtn = document.querySelector(".add-post-btn");




const loader = document.createElement('span');
loader.classList = 'loader';
document.getElementById('newsContainer').prepend(loader);



function onCharacterLoaded() {
    loader.style.display = "none";
};



function getUsersData() {
    return fetch(API_USER_URL, {
        method: 'GET',
    })
        .then(response => response.json())
        .then(json => {
            console.log("users", json);
            return json;
        }).catch((error) => console.log(error));
}

function getPostsData() {
    return fetch(API_POST_URL, {
        method: 'GET',
    })
        .then(response => response.json())
        .then(json => {
            console.log("posts", json);
            return json;
        }).catch((error) => console.log(error));
};

function sendPostsData(title, body) {
    return fetch(API_POST_URL, {
        method: 'POST',
        body: JSON.stringify({
            title: title,
            body: body,
            userId: authenticatedUser.id,

        }),
        headers: {
            'Content-Type': 'application/json'
        }
    })
        .then(response => response.json())
        .catch((error) => console.error("Error submitting post", error))
};



function deletePost(id) {
    return fetch(`${API_POST_URL}/${id}`, {
        method: 'DELETE',
    }).catch((error) => console.log(error));
};



function fetchData() {

    const postDataPromise = getPostsData().then((postArray) => {
        postArray.forEach(post => posts.push(new Post(
            post.id, post.userId, post.title, post.body))
        )
    }

    );

    const userDataPromise = getUsersData().then((userArray) => {
        userArray.forEach(user => users.push(new User(
            user.id, user.name, user.username, user.email)));
        authenticatedUser = users[0];
    }
    )

    const promiseTimeOut = new Promise((resolve, reject) => {
        console.log('Promise Pending');
        setTimeout(() => {
            resolve("Promise fullfilled");
        }, 1000);
    });

    return Promise.all([postDataPromise, userDataPromise, promiseTimeOut]);
}


function sendNewPost(title, body) {
    return sendPostsData().then(createdPost => posts.unshift(new Post(
        createdPost.id, authenticatedUser.id, title, body
    )))
}

function createCards() {
    newsContainer.innerHTML = "";
    posts.forEach(post => {

        const card = document.createElement('div');
        card.classList.add('card');
        newsContainer.append(card);

        const postTitle = document.createElement('h3');
        postTitle.classList.add('post-title');
        postTitle.innerText = post.title;

        const postText = document.createElement('p');
        postText.classList.add('post-text');
        postText.innerText = post.body;

        const deleteBtn = document.createElement('button');
        deleteBtn.innerText = "Delete Post"
        deleteBtn.classList.add('delete-btn');
        deleteBtn.addEventListener('click', () =>
            deletePost(post.id)
                .then((response) => response.status === 200 && card.remove())
                .then(() => console.log(`Card ${post.id} is deleted`))


        );
        card.append(postTitle, postText, deleteBtn);




        users.forEach(user => {
            if (post.userId === user.id) {

                const userEmail = document.createElement("p");
                userEmail.classList.add('user-email');
                userEmail.innerHTML = `user email: ${user.email}`;
                card.prepend(userEmail);

                const userName = document.createElement("h5");
                userName.classList.add('user-name');
                userName.innerHTML = `user name: ${user.username} ${user.name}`;
                card.prepend(userName);

            }

        })

    });
}




function createModalInput(id, type, placeholder, value) {
    const input = document.createElement("input");
    input.classList.add("input");
    input.id = id;
    input.type = type;
    input.placeholder = placeholder;
    return input
}

function createPostModal() {
    const modalPostForm = document.createElement('form');
    const newPostTitle = createModalInput("new-post-title", "text", "Enter your post title here");
    const newPostBody = createModalInput("new-post-body", "text", "Enter your post text here");
    const submitModalBtn = document.createElement("button");
    submitModalBtn.innerText = "Submit Post";

    modalPostForm.addEventListener('submit', (event) => {
        event.preventDefault();
        sendNewPost(newPostTitle.value, newPostBody.value)
            .then(smth => createCards())
            .finally(ignored => {
                postModal.style.display = "none";
                modalPostForm.reset();
            });
    });



    modalPostForm.append(newPostTitle, newPostBody, submitModalBtn);



    postModal.append(modalPostForm)
};

function createEditModal(post) {
    editModal.style.display = "none";
    const postEditForm = document.createElement('form');
    const editedPostTitle = createModalInput("new-post-title", "text", "Enter your post title here", post.title);
    const editedPostBody = createModalInput("new-post-body", "text", "Enter your post text here", post.body);

    const submitEditBtn = document.createElement("button");
    postEditForm.addEventListener('submit', (event) => {
        event.preventDefault();
        fetch(`${API_POST_URL}/${id}`, {
            method: 'PUT',
            body: JSON.stringify({
                userId: user.id,
                name: user.name,
                fullName: user.username,
            }),
            headers: {
                'Content-Type': 'application/json'
            }
        })
            .then(response => response.json())
            .then(json => console.log(json))
            .catch((error) => console.error("Error submitting post", error));


        postEditForm.reset();
    });
    postEditForm.append(editedPostTitle, editedPostBody, submitEditBtn);
    editModal.append(postEditForm);

}
createPostModal();
addPostBtn.addEventListener('click', () => {
    postModal.style.display = "block";
});





fetchData().then(smth => createCards()).then(ignored => onCharacterLoaded());


