export class Post {
    constructor(id, userId, title, body) {
        this.id = id;
        this.userId = userId,
            this.title = title;
        this.body = body;
    }
}

export class User {
    constructor(id, name, username, email) {
        this.id = id;
        this.name = name,
            this.username = username;
        this.email = email;
    }
}